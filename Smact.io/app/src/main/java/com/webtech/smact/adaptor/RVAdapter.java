package com.webtech.smact.adaptor;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.webtech.smact.R;
import com.webtech.smact.activity.TagViewer;

import java.util.ArrayList;

/**
 * Created by AbakashaP on 05-01-2016.
 */
public class RVAdapter extends RecyclerView.Adapter<RVAdapter.MyViewHolder> {

    private Context con;
    private ArrayList<String> listSampleIds;
    private static final String GETDATA_API = "http://59.144.5.166:5000/measuredvalue/";
    private String strInstrmentId;
    private ArrayList<String> sampleValues;
    private ArrayList<Boolean> SelectedValues;
    //private static RecyclerViewClickListener itemListener;

    public RVAdapter(Context con, ArrayList<String> strArrayItems, ArrayList<String> sampleValues, ArrayList<Boolean> SelectedValues) {
        this.con = con;
        this.listSampleIds = strArrayItems;
        // this.strInstrmentId = strInstrmentId;
        this.sampleValues = sampleValues;
        //this.itemListener = itemListener;
        strInstrmentId = strArrayItems.get(0);//first index will be instrument id
        this.SelectedValues = SelectedValues;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.itemview, parent, false);
        MyViewHolder viewHolder = new MyViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        Log.i("onBind position", holder.getOldPosition() + "");
        holder.tvSampleId.setText(listSampleIds.get(position));
        Log.i("Sample values", sampleValues.toString());
        if (sampleValues.get(position).equals("- -")) {
            holder.etValue.setText(sampleValues.get(position));
        } else {
            holder.etValue.setTextColor(con.getResources().getColor(R.color.text_black));
            holder.etValue.setText(sampleValues.get(position));
        }


        if (SelectedValues.get(position)) {
            holder.layout_linear.setBackground(con.getResources().getDrawable(R.color.selected_item));
        } else {
            holder.layout_linear.setBackground(con.getResources().getDrawable(R.color.un_selected_item));
        }
        boolean isNotFocused = true;
        for (int f = 0; f < SelectedValues.size(); f++) {
            if (SelectedValues.get(f)) {
                isNotFocused = false;
            }
        }
        if (isNotFocused && position == TagViewer.count) {
            holder.layout_linear.setBackground(con.getResources().getDrawable(R.color.selected_item));
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                hideKeybord();
                for (int i = 0; i < listSampleIds.size(); i++) {
                    SelectedValues.set(i, false);
                }
                TagViewer.focusedPosition = holder.getPosition();
                TagViewer.isfocused = true;
                Log.i("Sample id Position :", holder.getPosition() + "");
                SelectedValues.set(position, true);
                TagViewer.adapter.notifyDataSetChanged();

            }
        });

    }

    @Override
    public int getItemCount() {
        return listSampleIds.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        //EditText etValue;
        TextView tvSampleId, etValue;
        CardView cardView;
        LinearLayout layout_linear;

        //Button btnGet;
        public MyViewHolder(View itemView) {
            super(itemView);
//            etValue = (EditText) itemView.findViewById(R.id.etValue);
            etValue = (TextView) itemView.findViewById(R.id.etValue);
            tvSampleId = (TextView) itemView.findViewById(R.id.tvSampleId);
            cardView = (CardView) itemView.findViewById(R.id.idCardView);
            // btnGet = (Button) itemView.findViewById(R.id.btnGet);
            layout_linear = (LinearLayout) itemView.findViewById(R.id.layout_linear);
        }


    }

    public void hideKeybord() {
        InputMethodManager imm = (InputMethodManager) con.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(TagViewer.etBaseURL.getWindowToken(), 0);
    }
}
