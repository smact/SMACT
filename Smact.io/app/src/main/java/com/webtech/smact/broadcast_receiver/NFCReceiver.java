package com.webtech.smact.broadcast_receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.os.Parcelable;

public class NFCReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {
            Parcelable[] messages = intent
                    .getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            NdefMessage[] ndefmessages;
            if (messages != null) {
                ndefmessages = new NdefMessage[messages.length];

                for (int i = 0; i < messages.length; i++) {
                    ndefmessages[i] = (NdefMessage) messages[i];
                }

            }

        }

    }

}
