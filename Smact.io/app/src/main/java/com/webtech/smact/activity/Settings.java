package com.webtech.smact.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.webtech.smact.R;
import com.webtech.smact.model.Model;

public class Settings extends AppCompatActivity {

    Button mOKbnt;
    EditText mEt_ServerURL;
    SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        sp = getSharedPreferences(Model.Main_preferences, MODE_PRIVATE);


        mOKbnt = (Button) findViewById(R.id.btnOK);
        mEt_ServerURL = (EditText) findViewById(R.id.server_url_et);

        String strUrl = sp.getString(Model.SERVER_URL, "");
        if (!strUrl.equals("")) {
            mEt_ServerURL.setText(strUrl);
        }
        mOKbnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mEt_ServerURL.getText().toString().length() > 0) {

                    if (URLUtil.isValidUrl(mEt_ServerURL.getText().toString())) {
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString(Model.SERVER_URL, mEt_ServerURL.getText().toString());
                        editor.apply();
                        finish();
                    }else{
                        Toast.makeText(getApplicationContext(), "Please enter valid url", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please enter server url", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

}
