/*
 * Copyright (C) 2010 The Android Open Source Project
 * Copyright (C) 2011 Adam Nybäck
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.webtech.smact.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.nfc.Tag;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.MifareUltralight;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.webtech.smact.R;
import com.webtech.smact.adaptor.RVAdapter;
import com.webtech.smact.http_request.HTTP_GET;
import com.webtech.smact.http_request.HTTP_POST;
import com.webtech.smact.model.API_MODEL;
import com.webtech.smact.model.Model;
import com.webtech.smact.util.Connectivity;
import com.webtech.smact.util.RecyclerViewClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * An {@link Activity} which handles a broadcast of a new tag that the device just discovered.
 */
public class TagViewer extends AppCompatActivity implements RecyclerViewClickListener {

    private static final DateFormat TIME_FORMAT = SimpleDateFormat.getDateTimeInstance();
    //    private LinearLayout mTagContent;
    private static final String TAG = "TagViewer";
    private NfcAdapter mAdapter;
    private PendingIntent mPendingIntent;
    private NdefMessage mNdefPushMessage;

    private AlertDialog mDialog;
    private AlertDialog mTagTouchDialog;
    private RecyclerView recyclerView;
    private TextView tvInsID;
    private ArrayList<String> arrayListSampleID = new ArrayList<String>();
    private ArrayList<String> sampleValues = new ArrayList<String>();
    private ArrayList<Boolean> SelectedValues = new ArrayList<Boolean>();
    public static int count = 0;
    //    private static final String GETDATA_API = "http://raspberry-pi.vmokshagroup.com:8000/data";
    private String strInstrmentId;
    public static boolean isfocused = false;
    public static int focusedPosition;
    private boolean successStatus = false;
    public static RVAdapter adapter;
    private static int mTotalItems;
    private RecyclerViewClickListener itemListener;
    public static EditText etBaseURL;
    private boolean isInstrumentAdded = false;

    public static String strException = "";
    private ProgressDialog progressBar;
    private Dialog mDialogAlertPwd;
    private Dialog mDialogAlertNfc, mDialogAlertNfcEnable;
    private Button mButtonCancle, mBntCancle, mBntOk;
    private TextView mtxtInstremetId;
    private String instrumentURL;
    private String strJsonURL;
    SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tag_viewer);

        Log.e(TAG, "Application started");
        Toast.makeText(TagViewer.this, "Application Started", Toast.LENGTH_LONG).show();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        sp = getSharedPreferences(Model.Main_preferences, MODE_PRIVATE);
        Model.BASE_URL = sp.getString(Model.SERVER_URL, "");
        resolveIntent(getIntent());

        nfcCheck();
        mDialog = new AlertDialog.Builder(this).setNeutralButton("Ok", null).create();

        mAdapter = NfcAdapter.getDefaultAdapter(this);


        mPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        mNdefPushMessage = new NdefMessage(new NdefRecord[]{newTextRecord(
                "Message from NFC Reader :-)", Locale.ENGLISH, true)});
        ((Button) findViewById(R.id.btnGetValue)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeybord();
                if (isfocused) {
                    if (mtxtInstremetId != null && !mtxtInstremetId.getText().equals("")) {
                        if (getSampleValueForSampleId(arrayListSampleID.get(focusedPosition), focusedPosition)) {
                            isfocused = false;
                        } else {

                        }
                    } else {
                        Toast.makeText(TagViewer.this, "Please enter device id", Toast.LENGTH_LONG).show();
                    }
                } else {
                    if (arrayListSampleID.size() > 0) {
                        if (count < mTotalItems) {
                            if (mtxtInstremetId != null && !mtxtInstremetId.getText().equals("")) {
                                getSampleValueForSampleId(arrayListSampleID.get(count), count);
                            } else {
                                Toast.makeText(TagViewer.this, "Please enter device id", Toast.LENGTH_LONG).show();
                            }

                        } else {
                            Toast t = Toast.makeText(TagViewer.this, "Please focus to change sample value", Toast.LENGTH_SHORT);
                            t.show();
                        }
                    } else {
                        Toast t = Toast.makeText(TagViewer.this, "Please scan sample value", Toast.LENGTH_SHORT);
                        t.show();
                    }
                }
            }
        });
        recyclerView = (RecyclerView) findViewById(R.id.rv);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager lm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(lm);
        mtxtInstremetId = (TextView) findViewById(R.id.inst_id);

        progressBar = new ProgressDialog(this);
        progressBar.setTitle("               Please Wait ");
        progressBar.setMessage("Fetching data from Pi");
        progressBar.setCancelable(false);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        //Hard Coded
        // TODO: 15-03-2016
        strJsonURL = "{\"request\":{\"unitId\":\"Model A+\",\"deviceId\": \"Beamex RTS24\"}}";
        getUrl(strJsonURL);
    }

    private void nfcCheck() {
        NfcManager manager = (NfcManager) getSystemService(Context.NFC_SERVICE);
        NfcAdapter adapter = manager.getDefaultAdapter();
        if (adapter != null) {
            Log.e(TAG, "NFC Present in mobile");
            Toast.makeText(this, "Please touch the NFC tag", Toast.LENGTH_SHORT).show();
            return;

        } else {
            mDialogAlertNfc = new Dialog(TagViewer.this);
            mDialogAlertNfc.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mDialogAlertNfc.setContentView(R.layout.nfc_alert);
            mDialogAlertNfc.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mDialogAlertNfc.setCancelable(false);
            mDialogAlertNfc.show();
            mButtonCancle = (Button) mDialogAlertNfc.findViewById(R.id.bnt_cancle);

            mButtonCancle.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (mDialogAlertNfc.isShowing()) {
                        mDialogAlertNfc.dismiss();
                    }
                    finish();
                    System.exit(0);
                }
            });
        }
    }

    public void hideKeybord() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        isfocused = false;
        count = 0;
    }

    boolean getSampleValueForSampleId(String sampleId, final int position) {
        successStatus = false;
        HTTP_GET get = new HTTP_GET() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.show();
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progressBar.dismiss();
                if (s != null && !s.isEmpty()) {
                    try {
                        JSONObject jsonObject = new JSONObject(s);
                        String data = jsonObject.getString("last_Data");
                        if (isfocused) {
                            isfocused = false;
                            sampleValues.remove(position);
                            sampleValues.add(position, data);
                            count = position + 1;
                        } else {
                            int p = sampleValues.size() - 1;
                            sampleValues.remove(count);
                            sampleValues.add(count, data);
                            count++;
                        }

                        successStatus = true;
                        adapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        Toast.makeText(TagViewer.this, "Internal server error", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(TagViewer.this, "Internal server error", Toast.LENGTH_SHORT).show();

                }
            }
        };
//        String strBaseURL = etBaseURL.getText().toString().trim();
        String strBaseURL = instrumentURL;
        if (strBaseURL != null && !strBaseURL.isEmpty()) {
            if (((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null) {
                String strUrl = strBaseURL;
                get.execute(strUrl);
                for (int i = 0; i < arrayListSampleID.size(); i++) {
                    SelectedValues.set(i, false);
                }
            } else {
                Toast t = Toast.makeText(TagViewer.this, "Please check Internet connection", Toast.LENGTH_SHORT);
                t.show();
            }
        } else {
            Toast t = Toast.makeText(TagViewer.this, getResources().getString(R.string.device_not_registered), Toast.LENGTH_SHORT);
            t.show();
        }

        return successStatus;
    }

    private void setUpRecyclerView(final String json_sample) {
        Log.e(TAG, "NFC contains: " + json_sample);
        Toast.makeText(TagViewer.this, "NFC contains: " + json_sample, Toast.LENGTH_LONG).show();
        String dataString = null;
        boolean isJSONEXCEPTION = true;

        try {
            JSONObject jsonObject = new JSONObject(json_sample);
            if (json_sample.contains("sample id")) {
                dataString = jsonObject.getString("sample id");
                isInstrumentAdded = true;
                Log.e(TAG, "Sample ID" + dataString);
                Toast.makeText(TagViewer.this, "Sample ID: " + dataString, Toast.LENGTH_LONG).show();
                isJSONEXCEPTION = false;
            } else if (json_sample.contains("device id")) {
//                JSONObject obj = jsonObject.getJSONObject("request");
//                dataString = obj.getString("deviceId");
                dataString = jsonObject.getString("device id");
                Log.e(TAG, "Device ID" + dataString);
                Toast.makeText(TagViewer.this, "Device ID: " + dataString, Toast.LENGTH_LONG).show();
                isInstrumentAdded = false;
                isJSONEXCEPTION = false;
            } else {
                isJSONEXCEPTION = true;
            }

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, e.toString());
            isJSONEXCEPTION = true;
//            Toast.makeText(TagViewer.this, "NFC JSON ERROR: " + e.toString() + " line no 307", Toast.LENGTH_LONG).show();
        }
        if (!isJSONEXCEPTION) {
            boolean isSamplePresent = true;
            if (isInstrumentAdded) {
                if (arrayListSampleID.size() > 0) {
                    for (int k = 0; k < arrayListSampleID.size(); k++) {
                        if (dataString.contentEquals(arrayListSampleID.get(k))) {
                            isSamplePresent = true;
                            k = arrayListSampleID.size() - 1;
                        } else {
                            isSamplePresent = false;
                        }
                    }
                } else {
                    isSamplePresent = false;
                }
                if (!isSamplePresent) {
                    arrayListSampleID.add(dataString);
                    //TODO
                    mTotalItems = arrayListSampleID.size();
                    sampleValues.add("- -");
                    SelectedValues.add(false);
                    adapter = new RVAdapter(this, arrayListSampleID, sampleValues, SelectedValues);
                    recyclerView.setAdapter(adapter);
                    Toast.makeText(TagViewer.this, "Sample added to sample list", Toast.LENGTH_LONG).show();
                } else {

                    if (mDialogAlertPwd == null || !mDialogAlertPwd.isShowing()) {
                        mDialogAlertPwd = new Dialog(TagViewer.this);
                        mDialogAlertPwd.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        mDialogAlertPwd.setContentView(R.layout.dialog_accepted_service);
                        mDialogAlertPwd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        ((TextView) mDialogAlertPwd.findViewById(R.id.Message)).setText("The sample id already exists");
                        mDialogAlertPwd.setCancelable(false);
                        mDialogAlertPwd.show();
                        mButtonCancle = (Button) mDialogAlertPwd.findViewById(R.id.bnt_cancle);

                        mButtonCancle.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                if (mDialogAlertPwd.isShowing())
                                    mDialogAlertPwd.dismiss();

                            }
                        });
                    }
                }
            } else {
                if (mtxtInstremetId.getText().equals("") || !mtxtInstremetId.getText().equals(dataString)) {
                    mtxtInstremetId.setText(dataString);
                    strInstrmentId = dataString;
                    if (mtxtInstremetId.getText().length() > 0)
                        Toast.makeText(TagViewer.this, "Device added", Toast.LENGTH_LONG).show();
//                getUrl(json_sample);
                } else {
                    if (mDialogAlertPwd == null || !mDialogAlertPwd.isShowing()) {

                        mDialogAlertPwd = new Dialog(TagViewer.this);
                        mDialogAlertPwd.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        mDialogAlertPwd.setContentView(R.layout.dialog_accepted_service);
                        mDialogAlertPwd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        ((TextView) mDialogAlertPwd.findViewById(R.id.Message)).setText("The instrument id already exists");
                        mDialogAlertPwd.setCancelable(false);
                        mDialogAlertPwd.show();
                        mButtonCancle = (Button) mDialogAlertPwd.findViewById(R.id.bnt_cancle);

                        mButtonCancle.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                if (mDialogAlertPwd.isShowing())
                                    mDialogAlertPwd.dismiss();

                            }
                        });
                    }
                }
            }
        } else {
            Toast.makeText(TagViewer.this, "NFC tag does not contain valid data", Toast.LENGTH_LONG).show();
        }

    }

    private void getUrl(String json_sample) {
        Log.e(TAG, "Calling get URL API: " + json_sample);
        final ProgressDialog pd = new ProgressDialog(TagViewer.this);
        HTTP_POST post = new HTTP_POST() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if (!pd.isShowing()) {
                    pd.setMessage("Please wait");
                    pd.show();
                }
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                Log.e(TAG, "URL API result: " + s);
                if (pd.isShowing()) {
                    pd.cancel();
                }

                if (s != null && s.length() > 0) {
                    try {
                        JSONObject jsonObject = new JSONObject(s);
                        if (jsonObject.getBoolean("success")) {
                            //TODO
                            JSONArray array = jsonObject.getJSONArray("ViewModels");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject arrayObj = array.getJSONObject(i);
                                instrumentURL = arrayObj.getString("url");
                            }
                        } else {
                            Toast.makeText(TagViewer.this, getResources().getString(R.string.device_not_registered), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(TagViewer.this, getResources().getString(R.string.internal_server_error), Toast.LENGTH_LONG).show();
                }
            }
        };

        if (Connectivity.isConnected(TagViewer.this)) {
            if (!Model.BASE_URL.equals("")) {
                String a = Model.BASE_URL + API_MODEL.GET_URL;
                post.execute(Model.BASE_URL + API_MODEL.GET_URL, json_sample);
            } else {
                Intent i = new Intent(TagViewer.this, com.webtech.smact.activity.Settings.class);
                startActivityForResult(i, 2);
            }
        } else {
            Toast.makeText(TagViewer.this, getResources().getString(R.string.check_internet_connection), Toast.LENGTH_SHORT).show();
        }
    }


    private NdefRecord newTextRecord(String text, Locale locale, boolean encodeInUtf8) {
        byte[] langBytes = locale.getLanguage().getBytes(Charset.forName("US-ASCII"));

        Charset utfEncoding = encodeInUtf8 ? Charset.forName("UTF-8") : Charset.forName("UTF-16");
        byte[] textBytes = text.getBytes(utfEncoding);

        int utfBit = encodeInUtf8 ? 0 : (1 << 7);
        char status = (char) (utfBit + langBytes.length);

        byte[] data = new byte[1 + langBytes.length + textBytes.length];
        data[0] = (byte) status;
        System.arraycopy(langBytes, 0, data, 1, langBytes.length);
        System.arraycopy(textBytes, 0, data, 1 + langBytes.length, textBytes.length);

        return new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_TEXT, new byte[0], data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAdapter != null) {
            if (!mAdapter.isEnabled()) {
                showWirelessSettingsDialog();
            }
            //
            mAdapter.enableForegroundDispatch(this, mPendingIntent, null, null);
            mAdapter.enableForegroundNdefPush(this, mNdefPushMessage);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAdapter != null) {
            mAdapter.disableForegroundDispatch(this);
            mAdapter.disableForegroundNdefPush(this);
        }
    }

    private void showWirelessSettingsDialog() {


        mDialogAlertNfcEnable = new Dialog(TagViewer.this);
        mDialogAlertNfcEnable.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogAlertNfcEnable.setContentView(R.layout.enable_nfc);
        mDialogAlertNfcEnable.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        mDialogAlertNfcEnable.show();
        mDialogAlertNfcEnable.setCancelable(false);
        mBntCancle = (Button) mDialogAlertNfcEnable.findViewById(R.id.bnt_cancle);

        mBntCancle.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mDialogAlertNfcEnable.isShowing())
                    mDialogAlertNfcEnable.dismiss();

                finish();
                System.exit(0);

            }
        });
        mBntOk = (Button) mDialogAlertNfcEnable.findViewById(R.id.bnt_ok);

        mBntOk.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                startActivity(intent);
                if (mDialogAlertNfcEnable.isShowing())
                    mDialogAlertNfcEnable.dismiss();

            }
        });
        return;
    }

    private void resolveIntent(Intent intent) {
        String action = intent.getAction();
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            NdefMessage[] msgs;
            if (rawMsgs != null) {
                msgs = new NdefMessage[rawMsgs.length];
                for (int i = 0; i < rawMsgs.length; i++) {
                    msgs[i] = (NdefMessage) rawMsgs[i];
                }
            } else {
                // Unknown tag type
                byte[] empty = new byte[0];
                byte[] id = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID);
                Parcelable tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                byte[] payload = dumpTagData(tag).getBytes();
                NdefRecord record = new NdefRecord(NdefRecord.TNF_UNKNOWN, empty, id, payload);
                NdefMessage msg = new NdefMessage(new NdefRecord[]{record});
                msgs = new NdefMessage[]{msg};
            }
            // Setup the views
            buildTagViews(msgs);
        }
    }

    private String dumpTagData(Parcelable p) {
        StringBuilder sb = new StringBuilder();
        Tag tag = (Tag) p;
        byte[] id = tag.getId();
        sb.append("Tag ID (hex): ").append(getHex(id)).append("\n");
        sb.append("Tag ID (dec): ").append(getDec(id)).append("\n");
        sb.append("ID (reversed): ").append(getReversed(id)).append("\n");

        String prefix = "android.nfc.tech.";
        sb.append("Technologies: ");
        for (String tech : tag.getTechList()) {
            sb.append(tech.substring(prefix.length()));
            sb.append(", ");
        }
        sb.delete(sb.length() - 2, sb.length());
        for (String tech : tag.getTechList()) {
            if (tech.equals(MifareClassic.class.getName())) {
                sb.append('\n');
                MifareClassic mifareTag = MifareClassic.get(tag);
                String type = "Unknown";
                switch (mifareTag.getType()) {
                    case MifareClassic.TYPE_CLASSIC:
                        type = "Classic";
                        break;
                    case MifareClassic.TYPE_PLUS:
                        type = "Plus";
                        break;
                    case MifareClassic.TYPE_PRO:
                        type = "Pro";
                        break;
                }
                sb.append("Mifare Classic type: ");
                sb.append(type);
                sb.append('\n');

                sb.append("Mifare size: ");
                sb.append(mifareTag.getSize() + " bytes");
                sb.append('\n');

                sb.append("Mifare sectors: ");
                sb.append(mifareTag.getSectorCount());
                sb.append('\n');

                sb.append("Mifare blocks: ");
                sb.append(mifareTag.getBlockCount());
            }

            if (tech.equals(MifareUltralight.class.getName())) {
                sb.append('\n');
                MifareUltralight mifareUlTag = MifareUltralight.get(tag);
                String type = "Unknown";
                switch (mifareUlTag.getType()) {
                    case MifareUltralight.TYPE_ULTRALIGHT:
                        type = "Ultralight";
                        break;
                    case MifareUltralight.TYPE_ULTRALIGHT_C:
                        type = "Ultralight C";
                        break;
                }
                sb.append("Mifare Ultralight type: ");
                sb.append(type);
            }
        }

        return sb.toString();
    }

    private String getHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = bytes.length - 1; i >= 0; --i) {
            int b = bytes[i] & 0xff;
            if (b < 0x10)
                sb.append('0');
            sb.append(Integer.toHexString(b));
            if (i > 0) {
                sb.append(" ");
            }
        }
        return sb.toString();
    }

    private long getDec(byte[] bytes) {
        long result = 0;
        long factor = 1;
        for (int i = 0; i < bytes.length; ++i) {
            long value = bytes[i] & 0xffl;
            result += value * factor;
            factor *= 256l;
        }
        return result;
    }

    private long getReversed(byte[] bytes) {
        long result = 0;
        long factor = 1;
        for (int i = bytes.length - 1; i >= 0; --i) {
            long value = bytes[i] & 0xffl;
            result += value * factor;
            factor *= 256l;
        }
        return result;
    }

    void buildTagViews(NdefMessage[] msgs) {
        if (msgs == null || msgs.length == 0) {
            return;
        }
        if (!isfocused)
            isfocused = false;
        // count = 0;
        NdefRecord[] records = msgs[0].getRecords();
        for (NdefRecord record : records) {
            String s = new String(record.getPayload());
            Log.e(TAG, "NFC Scan response: " + s);
            if (s != null && !s.isEmpty()) {
                if (!s.contains("Tag ID (hex)"))
                    setUpRecyclerView(s);
                else
                    Toast.makeText(getApplicationContext(), "Please scan the tag again.", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        setIntent(intent);
        resolveIntent(intent);
    }

    @Override
    public void recyclerViewListClicked(View v, int position) {
        // adapter = new RVAdapter(TagViewer.this, this);
    }

    private void serviceAcceptedDialog(String Message) {


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.settings_menu, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings_menu:
                Intent i = new Intent(TagViewer.this, com.webtech.smact.activity.Settings.class);
                startActivityForResult(i, 2);
                ;
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 2) {
            Model.BASE_URL = sp.getString(Model.SERVER_URL, "");
            getUrl(strJsonURL);
        }
    }
}
