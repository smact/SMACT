package com.webtech.smact.util;

import android.view.View;

/**
 * Created by AbakashaP on 12-01-2016.
 */
public interface  RecyclerViewClickListener {
    public void recyclerViewListClicked(View v, int position);
}




